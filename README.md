[![Crowdin](https://d322cqt584bo4o.cloudfront.net/leafpic/localized.svg)](https://crowdin.com/project/leafpic)
[![Donate](https://img.shields.io/badge/donate-paypal-blue.svg)](https://www.paypal.me/HoraApps)

**PSA**: We are moving on [gitlab](https://gitlab.com/HoraApps/LeafPic), the repo on Github will remain as a mirror. Don't submit PR on gihub. 

# LeafPic
<img src="https://gitlab.com/HoraApps/LeafPic/raw/abdf97052596380b8d4b838c6ab4a7c1bf854522/app/src/main/res/drawable/leaf_pic.png" align="left" width="200" hspace="10" vspace="10">
LeafPic is a fluid, material-designed alternative gallery, it also is ad-free and open source under GPLv3 license. It doesn't miss any of the main features of a stock gallery, and we also have plans to add more useful features.<br/>

<br>These features include:
<ul>
<li>LIST THE FUTURE PLANNED FEATURES HERE</li>
</ul>

A gallery mobile application is an application that stores and organizes photos. LeafPic differs from other gallery applications since it is focused on being ad-free and allows users to edit their photos within the app and view photo metadata. Users also are able to edit the theme of the entire app for a high sense of personalization.

<div style="display:flex;" >
<a href="https://f-droid.org/app/org.horaapps.leafpic">
    <img src="https://f-droid.org/badge/get-it-on.png"
         alt="Get it on F-Droid" height="80">
</a>
<a href="https://play.google.com/store/apps/details?id=org.horaapps.leafpic">
    <img alt="Get it on Google Play"
        height="80"
        src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" />
</a>
</div>
</br></br>

## Screenshots
<div style="display:flex;" >
<img  src="screenshots/1.png" width="19%" >
<img style="margin-left:10px;" src="screenshots/2.png" width="19%" >
<img style="margin-left:10px;" src="screenshots/3.png" width="19%" >
<img style="margin-left:10px;" src="screenshots/4.png" width="19%" >
<img style="margin-left:10px;" src="screenshots/5.png" width="19%" >

</div>

#### Contributing

Read through the following article to get a sense of how you can contribute to open source projects in a meaningful way:    [link to article](https://www.freecodecamp.org/news/how-to-contribute-to-open-source-projects-beginners-guide/)

###### Testing
Do you want to be a tester? Join our Telegram group! Send a message to [@dnldsht](https://t.me/dnldsht) or [@CalvinNoronha](https://t.me/CalvinNoronha) we will add you.
We will release apks to test features or to check if bugs have been fixed.

###### Code 
If you are a developer and you wish to contribute to the app please fork the project
and submit a pull request on the [dev branch](https://gitlab.com/HoraApps/LeafPic/tree/dev).
If you want, you can join us on Telegram - send us a message we will add you!

###### Issues
You can trace the status of known issues [here](https://gitlab.com/HoraApps/LeafPic/issues),
also feel free to file a new issue (helpful description, screenshots and logcat are appreciated), or send me an [email](mailto:dnld.sht@gmail.com) if you have any questions.

###### Translations
If you are able to contribute with a new translation of a missing language or if you want to improve an existing one, we greatly appreciate any suggestion!
The project uses [Crowdin](https://crowdin.com/project/leafpic), a platform that allows anybody to contribute to translating the app

#### Licensing
LeafPic is licensed under the [GNU v3 Public License](https://gitlab.com/HoraApps/LeafPic/blob/dev/LICENSE).
In addition to the terms set by the GNU v3 Public License, we ask that if you use any code from this repository that you send us a message to let us know.

#### Running The App
It is recommended that you use Android Studio to build and run the app locally. Instructions for how to perform this can be found [here](https://developer.android.com/studio/run). 

#### Technologies Used
This project is built as a standard Android app in Java. This project also uses Gradle for build automation. Check out the FAQs document for specifics on how the app is used.

#### External Resources
The following resources could be useful when developing:
<ul>
<li><a href="https://developer.android.com/studio/intro">https://developer.android.com/studio/intro</a></li>
<li><a href="https://www.w3schools.blog/android-tutorial">https://www.w3schools.blog/android-tutorial</a></li>
<li><a href="https://docs.gradle.org/current/userguide/what_is_gradle.html">https://docs.gradle.org/current/userguide/what_is_gradle.html</a></li>
</ul>